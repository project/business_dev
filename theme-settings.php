<?php
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\file\Entity\File;

/**
 * @file
 * Theme settings in this file.
 */
/**
 * Implements hook_form_system_theme_settings_alter().
 */

function business_dev_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state)
{ if ($form['#attributes']['class'][0] == 'system-theme-settings') {
  $form['#attached']['library'][] = 'business_dev/theme.setting';


  $form['business_dev'] = [
    '#type'       => 'vertical_tabs',
    '#title'      => '<h3>' . t('Business development Theme Settings') . '</h3>',
    '#default_tab' => 'general',
  ];

  $form['general'] = [
    '#type'  => 'details',
    '#title' => t('General'),
    '#description' => t('<h3>Thank you for using business_dev theme</h3>'),
    '#group' => 'business_dev',
  ];


  $form['footer_logo'] = array(
    '#title' => t('Footer Logo'),
    '#description' => t('This Is Logo Footer: png|jpg|jpeg'),
    '#type' => 'details',
    '#group' => 'business_dev',
);
  $form['footer_logo']['logo_footer'] = array(
        '#title' => t('Logo Footer'),
        '#description' => t('This Is Logo Footer: png|jpg|jpeg'),
        '#type' => 'managed_file',
        '#upload_location' => 'public://logo-footer/',
        '#upload_validators' => array(
            'file_validate_extensions' => array('png jpg jpeg'),
        ),
        '#default_value' => theme_get_setting('logo_footer', 'business_dev'),
        '#group' => 'business_dev',
    );

    $form['contact'] = array(
      '#type' => 'details',
      '#title' => t('Contact'),
      '#group' => 'business_dev',
    );
    $form['contact']['contact_us'] = array(
        '#type' => 'fieldset',
        '#title' => t('Contact Us'),
      );

    $text_1 = theme_get_setting('address', 'business_dev');
    $form['contact']['contact_us']['address'] = array(
        '#type' => 'textfield',
        '#title' => t('Write address'),
        '#default_value' => isset($text_1) ? $text_1 : '',
        '#description' => t('Enter your address here.'),
      );
    $text_2 = theme_get_setting('phone_number', 'business_dev');
    $form['contact']['contact_us']['phone_number'] = array(
        '#type' => 'textfield',
        '#title' => t('Write  phone number'),
        '#default_value' => isset($text_2) ? $text_2 : '',
        '#description' => t('Enter your phone number here.'),
      );
    $text_3 = theme_get_setting('email_url', 'business_dev');
    $form['contact']['contact_us']['email_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Write Email Url'),
        '#default_value' => isset($text_3) ? $text_3 : '',
        '#description' => t('Enter your Email Url here.'),
      );
    $text_4 = theme_get_setting('email', 'business_dev');
    $form['contact']['contact_us']['email'] = array(
        '#type' => 'textfield',
        '#title' => t('Write Email'),
        '#default_value' => isset($text_4) ? $text_4 : '',
        '#description' => t('Enter your Email here.'),
      );

    // about company
    $form['company_info'] = array(
        '#type' => 'details',
        '#title' => t('Company Information'),
        '#group' => 'business_dev',
      );
      $form['company_info']['about_company'] = array(
        '#type' => 'fieldset',
        '#title' => t('About Company'),
      );
    $label = theme_get_setting('label', 'business_dev');
    $form['company_info']['about_company']['label'] = array(
        '#type' => 'textfield',
        '#title' => t('Write Label'),
        '#default_value' => isset($label) ? $label : '',
        '#description' => t('Enter label here.'),
      );

    $details = theme_get_setting('details', 'business_dev');
    $form['company_info']['about_company']['details'] = array(
        '#type' => 'textarea',
        '#title' => t('Write Details'),
        '#default_value' => isset($details) ? $details : '',
        '#description' => t('Enter details here.'),
      );
      $form['social_icons'] = array(
        '#type' => 'details',
        '#title' => t('Social Icons'),
        '#group' => 'business_dev',
      );

      $form['social_icons']['facebook_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Facebook URL'),
        '#default_value' => theme_get_setting('facebook_url'),
      );

      $form['social_icons']['twitter_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Twitter URL'),
        '#default_value' => theme_get_setting('twitter_url'),
      );

      $form['social_icons']['linkedin_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Linkedin URL'),
        '#default_value' => theme_get_setting('linkedin_url'),
      );

      $form['social_icons']['instagram_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Instagram URL'),
        '#default_value' => theme_get_setting('instagram_url'),
      );
      $form['slider'] = array(
        '#type' => 'details',
        '#title' => t('Top Slider'),
        '#group' => 'business_dev',
      );
      $form['slider']['heading'] = array(
        '#type' => 'textfield',
        '#title' => t('Heading'),
        '#default_value' => theme_get_setting('heading', 'business_dev'),
      );
      $form['slider']['discription'] = array(
        '#type' => 'textarea',
        '#title' => t('Description'),
        '#default_value' => theme_get_setting('discription', 'business_dev'),
      );
      $form['slider']['button_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Button Url'),
        '#default_value' => theme_get_setting('button_url', 'business_dev'),
      );
      $form['slider']['button_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Button Title'),
        '#default_value' => theme_get_setting('button_title', 'business_dev'),
      );


  $form['slider']['number_of_slides'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of slider images'),
    '#default_value' => theme_get_setting('number_of_slides', 'business_dev'),
  );

  $form['slider']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  $form['slider']['#submit'][] = 'business_dev_settings_form_submit';
  $number = theme_get_setting('number_of_slides', 'business_dev');
  if($number){
    for ($i = 1; $i <= $number; $i++) {
      $form['slider']['custom_image']['slide_image_' . $i] = [
        '#type' => 'managed_file',
        '#title' => t('Slide Image @num', ['@num' => $i]),
        '#description' => t('Upload slide image @num', ['@num' => $i]),
        '#default_value' => theme_get_setting('slide_image_' . $i, 'business_dev'),
        '#upload_location' => 'public://business_dev/slider/',
        '#upload_validators' => [
          'file_validate_extensions' => ['jpg jpeg png gif webp'],
        ],
      ];
    }
  }
  $form['copyright_section'] = [
    '#type' => 'details',
    '#title' => t('Footer copyright section'),
    '#group' => 'business_dev',
];
  $form['copyright_section']['copyright'] = [
    '#type' => 'details',
    '#title' => t('Footer copyright'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
];
     $form['copyright_section']['copyright']['copyright_text'] = [
    '#type' => 'textarea',
    '#title' => t('Write text with developer name'),
    '#default_value' => theme_get_setting('copyright_text', 'business_dev'),
  ];
// colors
  $form['colors_setting'] = [
    '#type' => 'details',
    '#title' => t('Set Colors'),
    '#group' => 'business_dev',
  ];

  $form['colors_setting']['color_options'] = [
    '#type' => 'details',
    '#title' => t('Set as you want'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['colors_setting']['color_options']['footer_bg_color'] = [
    '#type' => 'color',
    '#title' => t('footer bg Color'),
    '#default_value' => theme_get_setting('footer_bg_color', 'business_dev'),
    '#description' => t("Choose footer bg Color."),
  ];

}
}
/**
 * Form submission handler for the theme settings form.
 */
function business_dev_settings_form_submit(array &$form, \Drupal\Core\Form\FormStateInterface &$form_state) {
  // Get the value of the custom textfield.
  $custom_textfield_value = $form_state->getValue('number_of_slides');


  \Drupal::configFactory()->getEditable('business_dev.settings')
  ->set('number_of_slides', $custom_textfield_value)

  ->save();
}


